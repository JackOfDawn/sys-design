﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class MenuMain : MenuTemplate
    {
        public MenuMain()
            :base(6, "~Main Menu~")
        {
       
        }

        public override void displayMenu()
        {
            Console.WriteLine("1) New AddressBook");
            Console.WriteLine("2) Load AddressBook");
            Console.WriteLine("3) Save AddressBook");
            Console.WriteLine("4) New Contact");
            Console.WriteLine("5) Search");
            Console.WriteLine("6) Quit");
        }
    }
}
