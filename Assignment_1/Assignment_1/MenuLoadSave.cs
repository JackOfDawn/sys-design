﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class MenuLoadSave : MenuTemplate
    {
        public MenuLoadSave()
            :base(2, "~Select Format~")
        {
       
        }

        public override void displayMenu()
        {
            Console.WriteLine("1) Xml Format?");
            Console.WriteLine("2) Text Format?");
        }

    }
}
