﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class MenuSearch : MenuTemplate
    {
        public MenuSearch()
            :base(2, "~Search~")
        {
       
        }

        public override void displayMenu()
        {
            Console.WriteLine("1) Search by Name");
            Console.WriteLine("2) Search by Number");
        }
    }
}
