﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Contact
    {

        public string _Name;

        public string _Street;
        public string _City;
        public string _State;
        public string _Zip;

        public string _Number;

        public Contact()
        {

            _Street = "";
            _City = "";
            _State = "";
            _Zip = "";
            _Name = "";
            _Number = "";
        }

        public Contact(string name, string street, string city, string state, string zip, string number)
        {
            _Name = name;
            _Street = street;
            _City = city;
            _Number = number;
            _State = state;
            _Zip = zip;
        }

        public string display()
        {
            string tmpStr = "";

            tmpStr += _Name + Environment.NewLine +
                _Street + Environment.NewLine +
                _City + Environment.NewLine +
                _State + Environment.NewLine +
                _Zip + Environment.NewLine +
                _Number + Environment.NewLine;

            return tmpStr;
        }

        public static bool operator ==(Contact c1, Contact c2)
        {
            if (c1._Number == c2._Number)
                return true;
            if (c1._Name == c2._Name)
                return true;
            if (c1._Street == c2._Street)
                return true;
            if (c1._City == c2._City)
                return true;
            if (c1._State == c2._State)
                return true;
            if (c1._Zip == c2._Zip)
                return true;
            return false;
        }

        public static bool operator !=(Contact c1, Contact c2)
        {
            if (c1._Number != c2._Number)
                return true;
            if (c1._Name != c2._Name)
                return true;
            if (c1._Street != c2._Street)
                return true;
            if (c1._City != c2._City)
                return true;
            if (c1._State != c2._State)
                return true;
            if (c1._Zip != c2._Zip)
                return true;
            return false;
        }

        

        
    }
}
