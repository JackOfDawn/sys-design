﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class Program
    {
        static Manager myManager = new Manager();
        static bool quit;
        static int menuStructure;
        static char menuSelection;
       

        static MenuTemplate baseMenu;
        static MenuMain mainMenu;
        static MenuLoadSave loadSaveMenu;
        static MenuSearch searchMenu;

        static void Main(string[] args)
        {
            quit = false;
            myManager = new Manager();

      
            mainMenu = new MenuMain();
            loadSaveMenu = new MenuLoadSave();
            searchMenu = new MenuSearch();

            baseMenu = mainMenu;



          
            menuStructure = 0;


            while (!quit)
            {
                Console.Clear();


                baseMenu.displayTitle();
                baseMenu.displayMenu();
                menuSelection = getSelection();
                Console.Clear();
                if (!baseMenu.checkSelection(menuSelection))
                {
                    Console.WriteLine("INVALID INPUT! PRESS ANY KEY");
                    Console.ReadKey(true);
                    Console.Clear();
                    continue;
                }

                switch (menuStructure)
                {
                    case 0: //MainMenu
                        handleMainMenu();
                        break;
                    case 1:
                        handleLoading();
                        break;
                    case 2:
                        handleSaving();
                        break;
                    case 3:
                        handleSearch();
                        break;

                }


                 
            }
            
        }

        private static void handleSearch()
        {
            baseMenu.displayTitle();
            switch (menuSelection)
            {
                case '1':
                    myManager.searchNDisplayByName();
                    break;
                case '2':
                    myManager.searchNDisplayNumber();
                    break;
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey(true);
            Console.Clear();

            menuStructure = 0;
            baseMenu = mainMenu;
        }

        private static void handleSaving()
        {
            switch (menuSelection)
            {
                case '1':
                    myManager.saveBookXml();
                    break;
                case '2':
                    myManager.saveBookTxt();
                    break;
            }

            Console.WriteLine("Saved, Press any key to continue");
            Console.ReadKey(true);
            Console.Clear();

            menuStructure = 0;
            baseMenu = mainMenu;
        }

        private static void handleLoading()
        {
            Console.WriteLine("Enter File Name:");
            string tmpStr = Console.ReadLine();
            var loaded = true;
            switch (menuSelection)
            {
                case '1': //xml
                    if (!(myManager.loadBookXml(tmpStr)))
                        loaded = false;
                    break;
                case '2': //text
                    if (!(myManager.loadBookTxt(tmpStr)))
                        loaded = false;
                    break;
            }

            if(loaded)
                Console.WriteLine("Address Book Loaded");
            else
                Console.WriteLine("ERROR: File Not Found");

            Console.WriteLine("Press a key to continue");
            Console.ReadKey(true);
            Console.Clear();

            menuStructure = 0;
            baseMenu = mainMenu;
        }
        static public void handleMainMenu()
        {
           
            switch (menuSelection)
            {
                case '1': //new book
                    myManager.createNewBook();
                    break;
                case '2': //load
                    menuStructure = 1;
                    baseMenu = loadSaveMenu;
                    break;
                case '3': //save
                    menuStructure = 2;
                    baseMenu = loadSaveMenu;
                    break;
                case '4': //new contact
                    myManager.createNewContact();
                    break;
                case '5': //search
                    menuStructure = 3;
                    baseMenu = searchMenu;
                    break;
                case '6': // quit
                    quit = true;
                    menuStructure = 999;
                    break;
            }
        }

        static public char getSelection()
        {
            ConsoleKeyInfo keyPressed;
            char userSelection;
            keyPressed = Console.ReadKey(true);
            userSelection = keyPressed.KeyChar;
            return userSelection;

        }

        static public void quitSystem()
        {
            quit = true;
        }
    }
}
