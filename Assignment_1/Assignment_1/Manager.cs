﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Manager
    {
        private AddressBook _AddressBook;


        public Manager()
        {
            _AddressBook = new AddressBook();
        }

        public bool addContact(Contact contact)
        {
            return _AddressBook.addContact(contact);
        }

        public void createNewBook()
        {
            Console.WriteLine("Enter Address Book Name: ");
            string tmpStr = Console.ReadLine();
            newBook(tmpStr);

            Console.WriteLine("New Address book Made, Press Any Key...");
            Console.ReadKey(true);
        }

        public void createNewContact()
        {
            Console.WriteLine("Enter  Name: ");
            string tmpName = Console.ReadLine();
            Console.WriteLine("Enter  Street: ");
            string tmpStreet = Console.ReadLine();
            Console.WriteLine("Enter  City: ");
            string tmpCity = Console.ReadLine();
            Console.WriteLine("Enter  State: ");
            string tmpState = Console.ReadLine();

            Console.WriteLine("Enter  Zip: ");
            string tmpZip = Console.ReadLine();
            while (tmpZip.Length != 5)
            {
                Console.WriteLine("Enter Zip(5 Digits); ");
                tmpZip = Console.ReadLine();
            }

            Console.WriteLine("Enter Phone Number: ");
            string tmpNumber = Console.ReadLine();
            while (tmpNumber.Length != 10)
            {
                Console.WriteLine("Enter Phone Number(10 Digits): ");
                tmpNumber = Console.ReadLine();
            };

            Contact tmpContact = new Contact(tmpName, tmpStreet, tmpCity, tmpState, tmpZip, tmpNumber);

            if (addContact(tmpContact))
            {
                Console.WriteLine("Contact Added");
            }
            else
            {
                Console.WriteLine("Contact NOT Added: Already Exists");
            }

            Console.WriteLine("Press a key to continue...");
            Console.ReadKey(true);
            Console.Clear();
           
        }

        public bool loadBookTxt(string path)
        {


            if (!System.IO.File.Exists(path + ".txt"))
            {
                return false;
            }

            System.IO.StreamReader inputFile = new System.IO.StreamReader(path + ".txt");
            string tmpName = "", tmpStreet = "", tmpCity = "", tmpState = "", tmpZip = "", tmpNumber = "";
            Contact tmpContact;


            tmpName = inputFile.ReadLine();

            newBook(path);

            while (!inputFile.EndOfStream)
            {
                tmpStreet = inputFile.ReadLine();
                tmpCity = inputFile.ReadLine();
                tmpState = inputFile.ReadLine();
                tmpZip = inputFile.ReadLine();
                tmpNumber = inputFile.ReadLine();

                tmpContact = new Contact(tmpName, tmpStreet, tmpCity, tmpState, tmpZip, tmpNumber);

                _AddressBook.addContact(tmpContact);

                tmpName = inputFile.ReadLine();
            }

            inputFile.Close();


            return true;
        }

        public bool loadBookXml(string path)
        {
            if (!System.IO.File.Exists(path + ".xml"))
            {
                return false;
            }

            newBook(path);

            System.Xml.Serialization.XmlSerializer reader =
                new System.Xml.Serialization.XmlSerializer(typeof(AddressBook));

            System.IO.StreamReader inputFile = new System.IO.StreamReader(path + ".xml");

            _AddressBook = (AddressBook)reader.Deserialize(inputFile);

            inputFile.Close();

            return true;
        }

        private void newBook(string path)
        {
           _AddressBook = new AddressBook(path);
        }


        public void saveBookTxt()
        {
            //System.IO.File.Create(_BookPath);
            System.IO.File.WriteAllText(_AddressBook.getPath() + ".txt" , _AddressBook.display());

        }

        public void saveBookXml()
        {
           
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(AddressBook));

            System.IO.StreamWriter outputFile = new System.IO.StreamWriter(_AddressBook.getPath() + ".xml");
            writer.Serialize(outputFile, _AddressBook);
            outputFile.Close();

        }


        public void searchNDisplayByName()
        {
            Console.WriteLine("Enter  Name: ");
            string tmpName = Console.ReadLine();

            var contacts = _AddressBook._Contacts.Where(c => c._Name == tmpName);

            var contactArray = contacts.ToArray();

            Console.Clear();

            Console.WriteLine("Found: " + Environment.NewLine);

            foreach (Contact c in contactArray)
            {
                Console.WriteLine(c.display());
            }

  
        }

        public void searchNDisplayNumber()
        {
            Console.WriteLine("Enter  Number: ");
            string tmpName = Console.ReadLine();

            var contacts = _AddressBook._Contacts.Where(c => c._Number == tmpName);

            var contactArray = contacts.ToArray();

            Console.Clear();

            Console.WriteLine("Found: " + Environment.NewLine);

            foreach (Contact c in contactArray)
            {
                Console.WriteLine(c.display());
            }
        }
    }
}
