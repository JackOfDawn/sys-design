﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class AddressBook
    {
        public List<Contact> _Contacts;
        public string _BookPath;
    

        public AddressBook()
        {
            _Contacts = new List<Contact>();
            _BookPath = "";
        }

        public AddressBook(string path)
        {
            _Contacts = new List<Contact>();
            _BookPath = path;
        }

        public bool addContact(Contact c1)
        {
            foreach (Contact contact in _Contacts)
            {
                if (c1 == contact)
                {
                    return false;
                }
            }

            _Contacts.Add(c1);
            return true;
        }

        public string display()
        {
            string tmpStr = "";

            foreach (Contact contact in _Contacts)
            {
                tmpStr += contact.display();
            }

            return tmpStr;
        }

        public string getPath()
        {
            return _BookPath;
        }
    }
}
