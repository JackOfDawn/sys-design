﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class MenuTemplate
    {

        private int _NumOptions;
        private string _Title;

        public MenuTemplate(int numItems, string menuTitle)
        {
            _NumOptions = numItems;
            _Title = menuTitle;
        }

        public virtual void displayMenu() { }
        public void displayTitle()
        {
            Console.WriteLine("\t\t\t" + _Title);
        }

        public bool checkSelection(char selection)
        {

            int choice = (int)Char.GetNumericValue(selection);
            
            bool valid = false;

            if(choice > 0 && choice <= _NumOptions)
                valid = true;


            return valid;
        }
    }
}
